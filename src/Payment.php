<?php

namespace ShopExpress\Tinkoff;

use JsonException;
use RuntimeException;

/**
 * Class Payment
 * @package ShopExpress\Tinkoff
 */
class Payment
{
    public const CONFIRMED = 'CONFIRMED';
    public const AUTHORIZED = 'AUTHORIZED';
    public const CANCELED = 'CANCELED';
    public const REJECTED = 'REJECTED';
    public const REVERSED = 'REVERSED';
    public const REFUNDED = 'REFUNDED';

    /**
     * @var string
     */
    private string $apiUrl;
    /**
     * @var string
     */
    private string $terminalKey;
    /**
     * @var string
     */
    private string $secretKey;
    /**
     * @var string
     */
    private string $paymentId;
    /**
     * @var string
     */
    private string $status;
    /**
     * @var string
     */
    private string $error;
    /**
     * @var string
     */
    private string $response;
    /**
     * @var string
     */
    private string $paymentUrl;

    /**
     * Payment constructor.
     *
     * @param string $terminalKey
     * @param string $secretKey
     */
    public function __construct(string $terminalKey, string $secretKey)
    {
        $this->apiUrl = 'https://securepay.tinkoff.ru/v2/';
        $this->terminalKey = $terminalKey;
        $this->secretKey = $secretKey;
    }

    /**
     * @return string
     */
    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * @return string
     */
    public function getResponse(): string
    {
        return $this->response;
    }

    /**
     * @return string
     */
    public function getPaymentUrl(): string
    {
        return $this->paymentUrl;
    }

    /**
     * @param string $name
     *
     * @throws JsonException
     * @return string
     */
    public function get(string $name): string
    {
        if ($this->response && $json = json_decode($this->response, true, 512, JSON_THROW_ON_ERROR)) {
            foreach ($json as $key => $value) {
                if (strtolower($name) === strtolower($key)) {
                    return $value;
                }
            }
        }

        return false;
    }

    /**
     * @param $args mixed You could use associative array or url params string
     *
     * @throws JsonException
     * @return bool
     */
    public function init($args)
    {
        return $this->buildQuery('Init', $args);
    }

    /**
     * @param $args
     *
     * @return bool|string
     * @throws JsonException
     */
    public function getState($args)
    {
        return $this->buildQuery('GetState', $args);
    }

    /**
     * @param $args
     *
     * @return bool|string
     * @throws JsonException
     */
    public function confirm($args)
    {
        return $this->buildQuery('Confirm', $args);
    }

    /**
     * @param $args
     *
     * @return bool|string
     * @throws JsonException
     */
    public function charge($args)
    {
        return $this->buildQuery('Charge', $args);
    }

    /**
     * @param $args
     *
     * @return bool|string
     * @throws JsonException
     */
    public function addCustomer($args)
    {
        return $this->buildQuery('AddCustomer', $args);
    }

    /**
     * @param $args
     *
     * @return bool|string
     * @throws JsonException
     */
    public function getCustomer($args)
    {
        return $this->buildQuery('GetCustomer', $args);
    }

    /**
     * @param $args
     *
     * @return bool|string
     * @throws JsonException
     */
    public function removeCustomer($args)
    {
        return $this->buildQuery('RemoveCustomer', $args);
    }

    /**
     * @param $args
     *
     * @return bool|string
     * @throws JsonException
     */
    public function getCardList($args)
    {
        return $this->buildQuery('GetCardList', $args);
    }

    /**
     * @param $args
     *
     * @return bool|string
     * @throws JsonException
     */
    public function removeCard($args)
    {
        return $this->buildQuery('RemoveCard', $args);
    }

    /**
     * Builds a query string and call sendRequest method.
     * Could be used to custom API call method.
     *
     * @param string $path API method name
     * @param mixed $args query params
     *
     * @throws JsonException
     * @return bool|string
     */
    public function buildQuery(string $path, $args)
    {
        $url = $this->apiUrl;
        if (is_array($args)) {
            if (!array_key_exists('TerminalKey', $args)) {
                $args['TerminalKey'] = $this->terminalKey;
            }
            if (!array_key_exists('Token', $args)) {
                $args['Token'] = $this->genToken($args);
            }
        }
        $url = $this->_combineUrl($url, $path);


        return $this->_sendRequest($url, $args);
    }

    /**
     * Generates Token
     *
     * @param $args
     *
     * @return string
     */
    public function genToken($args): string
    {
        $token = '';
        $args['Password'] = $this->secretKey;
        ksort($args);

        foreach ($args as $arg) {
            if (!is_array($arg)) {
                $token .= $arg;
            }
        }

        return hash('sha256', $token);
    }

    /**
     * Combines parts of URL. Simply gets all parameters and puts '/' between
     *
     * @return string
     */
    private function _combineUrl(): string
    {
        $args = func_get_args();
        $url = '';
        foreach ($args as $arg) {
            if (is_string($arg)) {
                if ($arg[strlen($arg) - 1] !== '/') $arg .= '/';
                $url .= $arg;
            } else {
                continue;
            }
        }

        return $url;
    }

    /**
     * Main method. Call API with params
     *
     * @param $api_url
     * @param $args
     *
     * @throws JsonException
     * @return bool|string
     */
    private function _sendRequest($api_url, $args)
    {
        $this->error = '';
        if (is_array($args)) {
            $args = json_encode($args, JSON_THROW_ON_ERROR);
        }

        if (!($curl = curl_init())) {
            throw new RuntimeException('Can not create connection to ' . $api_url . ' with args ' . $args, 404);
        }

        curl_setopt($curl, CURLOPT_URL, $api_url);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $args);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
        ]);

        $out = curl_exec($curl);
        $this->response = $out;
        $json = json_decode($out, true, 512, JSON_THROW_ON_ERROR);

        if ($json) {
            if ($json['ErrorCode'] !== "0") {
                $this->error = $json['Details'];
            } else {
                $this->paymentUrl = $json['PaymentURL'];
                $this->paymentId = $json['PaymentId'];
                $this->status = $json['Status'];
            }
        }

        curl_close($curl);

        return $out;
    }
}