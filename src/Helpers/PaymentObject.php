<?php

namespace ShopExpress\Tinkoff\Helpers;

/**
 * Class PaymentObject
 */
class PaymentObject
{
    public const COMMODITY = 'commodity'; //Товар
    public const EXCISE = 'excise'; //Подакцизный товар
    public const JOB = 'job'; //Работа
    public const SERVICE = 'service'; //Услуга
    public const GAMBLING_BET = 'gambling_bet'; //Ставка азартной игры
    public const GAMBLING_PRIZE = 'gambling_prize'; //Выигрыш азартной игры
    public const LOTTERY = 'lottery'; //Лотерейный билет
    public const LOTTERY_PRIZE = 'lottery_prize'; //Выигрыш лотереи
    public const INTELLECTUAL_ACTIVITY = 'intellectual_activity'; //Предоставление результатов интеллектуальной деятельности
    public const PAYMENT = 'payment'; //Платеж
    public const AGENT_COMMISSION = 'agent_commission'; //Агентское вознаграждение
    public const COMPOSITE = 'composite'; //Составной предмет расчета
    public const ANOTHER = 'another'; //Иной предмет расчета
}