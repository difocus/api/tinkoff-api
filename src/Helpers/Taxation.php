<?php

namespace ShopExpress\Tinkoff\Helpers;

/**
 * Class Taxation
 */
class Taxation
{
    public const OSN = 'osn'; // Общая СН
    public const USN_INCOME = 'usn_income'; // Упрощенная СН (доходы)
    public const USN_INCOME_OUTCOME = 'usn_income_outcome'; // Упрощенная СН (доходы минус расходы)
    public const ENVD = 'envd'; // Единый налог на вмененный доход
    public const ESN = 'esn'; // Единый сельскохозяйственный налог
    public const PATENT = 'patent'; // Патентная СН
}