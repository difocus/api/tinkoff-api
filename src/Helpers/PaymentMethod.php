<?php

namespace ShopExpress\Tinkoff\Helpers;

/**
 * Class PaymentMethod
 */
class PaymentMethod
{
    public const FULL_PREPAYMENT = 'full_prepayment'; //Предоплата 100%
    public const PREPAYMENT = 'prepayment'; //Предоплата
    public const ADVANCE = 'advance'; //Аванc
    public const FULL_PAYMENT = 'full_payment'; //Полный расчет
    public const PARTIAL_PAYMENT = 'partial_payment'; //Частичный расчет и кредит
    public const CREDIT = 'credit'; //Передача в кредит
    public const CREDIT_PAYMENT = 'credit_payment'; //Оплата кредита
}