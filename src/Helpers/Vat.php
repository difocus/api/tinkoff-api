<?php

namespace ShopExpress\Tinkoff\Helpers;

/**
 * Class Vat
 */
class Vat
{
    public const NONE = 'none'; // Без НДС
    public const VAT0 = 'vat0'; // НДС 0%
    public const VAT10 = 'vat10'; // НДС 10%
    public const VAT20 = 'vat20'; // НДС 20%
}